package test.com.randomizerApp.memory;

import main.com.randomizerApp.memory.GenerateRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class GenerateRandomTest {

    GenerateRandom cut;
   ArrayList<Integer> excludeList = new ArrayList<>(Arrays.asList(6, 7, 8));

    static List<Arguments> getNextRandomTestArgs(){
        return List.of(
          Arguments.arguments(5,10),
          Arguments.arguments(1,10)
        );
    }

    @ParameterizedTest
    @MethodSource("getNextRandomTestArgs")
    void getNextRandomTest(int start, int end){
        cut = new GenerateRandom(start,end,excludeList);
        int actual = cut.getNextRandom();
        Assertions.assertTrue(true, String.valueOf(!excludeList.contains(actual)));
    }
}